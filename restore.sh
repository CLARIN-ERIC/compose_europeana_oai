VERBOSE=0

#
# Process script arguments and parse all flags
#
for i in "$@"
do
case $i in
    -v|--verbose)
        VERBOSE=1
        ;;
esac
#shift # past argument or value
done

if [ "${COMPOSE_DIR}" == "" ]; then
    echo "Backup script compose directory not set (COMPOSE_DIR)"
    exit 1
fi
if [ "${RESTORE_FILE}" == "" ]; then
    echo "Restore file not set (RESTORE_FILE)"
    exit 1
fi

#
# Script backup / restore functionality
# Restore the file ${RESTORE_FILE} of implement custom logic.
#

START_DIR="$(pwd)"
(
	cd "${COMPOSE_DIR}/.."
	
	SCRIPT_OPTS=()
	if [ "${VERBOSE}" == '1' ]; then
		SCRIPT_OPTS+="-v"
		echo "PWD: $(pwd)"
		echo "RESTORE_FILE: ${RESTORE_FILE} (=$(realpath ${RESTORE_FILE}))"
		echo "SCRIPT_OPTS: ${SCRIPT_OPTS}"
	fi

	if [ -e "${RESTORE_FILE}" ]; then
		RESTORE_FILE="$(realpath "${RESTORE_FILE}")"
		if [ "${VERBOSE}" == '1' ]; then
			echo "Restore file found! ${RESTORE_FILE}"
		fi
	else
		echo "Error: restore file not found ${RESTORE_FILE}"
		cd "${START_DIR}"
		exit 1
	fi

	(
		cd "${COMPOSE_DIR}/clarin" &&
		docker-compose -f docker-compose.yml -f backup-restore.yml \
			run --rm restore "${SCRIPT_OPTS}" 
	)
)