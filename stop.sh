#!/bin/bash
(
	if [ "${COMPOSE_DIR}" == "" ]; then
		COMPOSE_DIR=$(find . -maxdepth 2 -name 'clarin' -type d -exec bash -c 'if [ -f $1/docker-compose.yml ]; then dirname $1; fi' shell {} \;)
	fi
	
	COMPOSE_CMD=()
	COMPOSE_CMD+=("down")
	if [ "${COMPOSE_CMD_ARGS}" ]; then
		COMPOSE_CMD+=("${COMPOSE_CMD_ARGS}")
	fi

	cd "${COMPOSE_DIR}/clarin"
	docker-compose \
		-f docker-compose.yml \
		-f harvester.yml \
		-f aggregator.yml \
		"${COMPOSE_CMD[@]}"
)
