VERBOSE=0

#
# Process script arguments and parse all flags
#
for i in "$@"
do
case $i in
    -v|--verbose)
        VERBOSE=1
        ;;
esac
#shift # past argument or value
done

if [ "${COMPOSE_DIR}" == "" ]; then
    echo "Backup script compose directory not set (COMPOSE_DIR)"
    exit 1
fi
if [ "${BACKUPS_DIR}" == "" ]; then
    echo "Backup script backups directory not set (BACKUPS_DIR)"
    exit 1
fi
if [ "${BACKUPS_TIMESTAMP}" == "" ]; then
    echo "Backup timestamps not set (BACKUPS_TIMESTAMP)"
    exit 1
fi

#
# Script backup / restore functionality
# Restore the file ${RESTORE_FILE} of implement custom logic.
#
export BACKUPS_DIR="$(realpath "${BACKUPS_DIR}")"
export BACKUP_FILE_NAME="backup-${BACKUPS_TIMESTAMP}"

SCRIPT_OPTS=()
if [ "${VERBOSE}" == '1' ]; then
	SCRIPT_OPTS+="-v"
	echo "pwd: $(pwd)"
	echo "COMPOSE_DIR: ${COMPOSE_DIR}"
	echo "BACKUPS_DIR: ${BACKUPS_DIR}"
	echo "BACKUP_FILE_NAME: ${BACKUP_FILE_NAME}"
	echo "SCRIPT_OPTS: ${SCRIPT_OPTS}"
fi

(
	cd "${COMPOSE_DIR}/clarin" &&
	docker-compose -f docker-compose.yml -f backup-restore.yml \
		run --rm backup "${SCRIPT_OPTS}"
)
