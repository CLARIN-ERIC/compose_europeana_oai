#!/bin/bash
BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
COMPOSE_DIR="${BASE_DIR}/clarin"

main() {
	case "${1}" in
	  '--help')
	    usage
	    ;;
	  'run-fulltext-aggregator')
	    shift
	    do_run_fulltext_aggregator "$@"
	    ;;
	  'run-harvester')
	    shift
	    do_run_harvester "$@"
	    ;;
	  *)
	    echo "Unknown command: ${1}"
	    usage
	    exit 1
	    ;;
	esac	  
}

do_run_harvester() {
	_run_service europeana-harvester "$@"
}

do_run_fulltext_aggregator() {
	if [ "$#" -lt 1 ]; then
		echo "Error: please pass a space separated list of collection identifiers "
		usage
		exit 1
	fi
	echo "Will process $# collection(s): $@"
	for COLLECTION_ID in "$@"; do
		_run_service europeana-fulltext-aggregator retrieve "${COLLECTION_ID}" \
		&& _run_service europeana-fulltext-aggregator aggregate "${COLLECTION_ID}"
		
		_run_service europeana-fulltext-aggregator clean "${COLLECTION_ID}"
	done
}

_run_service() {
	(cd "${COMPOSE_DIR}" && \
		docker-compose \
			-f docker-compose.yml \
			-f harvester.yml -f aggregator.yml \
			run --rm "$@")
}

usage() {
	echo ""
	echo "    run-harvester                                         Run the Europeana OAI-PMH harvest"
	echo "    run-fulltext-aggregator <collection identifiers>      Run the full text aggregation"
	echo "                                                          (pass a space separated list of collection identifiers)"
}

main "$@"
