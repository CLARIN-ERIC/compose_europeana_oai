# CLARIN - Europeana aggregator & OAI-PMH provider

This compose project defines a set of services that fulfil two objectives in the [CLARIN](https://www.clarin.eu)
infrastructure and its partnership with [Europeana](https://www.europeana.eu):
1. Collect and aggregate metadata from Europeana services and from this derive metadata in the
[CMDI](https://www.clarin.eu/cmdi) format
2. Make the resulting metadata available for retrieval ('harvesting') through the
[OAI-PMH protocol](https://www.openarchives.org/pmh/)

This project was designed to be used with the [CLARIN control script](https://gitlab.com/CLARIN-ERIC/control-script/).
However, it can also be used as a regular docker compose project. See below for usage instructions.

## Overview of services

### OAI-PMH provider
The service `joai` defined in `clarin/docker-compose.yml` uses the CLARIN
[docker-joai](https://gitlab.com/CLARIN-ERIC/docker-joai/) image to provide an OAI-PMH endpoint. It must be
configured to serve the metadata records produced by the aggregation and harvesting services (see below). 

An additional service `nginx` is defined in the same Docker Compose configuration. It uses a CLARIN
[docker-alpine-nginx](https://gitlab.com/CLARIN-ERIC/docker-alpine-nginx) image to provide an HTTP server that
does two things: 1) securely proxy the JOAI service described above, and 2) give direct external access to the metadata
resources produced by the aggregation and harvesting services.

The `joai` and `nginx` services are intended to stay running in detached mode. 

The `nginx` service should be mapped to an available network port that is externally accessible as described in the
configuration instructions in this file.

### Metadata content collection, aggregation and generation
Two services are defined, both of which are intended to run as jobs (not permanent services) and produce CMDI metadata
which is to be served by the OAI-PMH provider of the `joai` service.

The first service is `europeana-harvester`, defined in `clarin/harvester.yml`. The 
[docker-europeana-harvest](https://gitlab.com/CLARIN-ERIC/docker-europeana-harvest) image is a customisation of the
[CLARIN OAI-PMH harvester](https://gitlab.com/CLARIN-ERIC/docker-oai-harvester). When started it carries out a harvest
of the [Europeana OAI-PMH endpoint](https://pro.europeana.eu/page/harvesting-and-downloads) according to a customisable
configuration. Part of the process is a 'pre-flight' which uses other Europeana APIs to determine precisely which
records to harvest. After completion of the harvest process, the results are stored in a persistent volume that is
shared with the `joai` and `nginx` services, which are responsible for making the metadata available.

The second service is `europeana-fulltext-aggregator`, defined in `clarin/aggregator.yml`. It uses the
[docker-europeana-fulltext-aggregations](https://gitlab.com/CLARIN-ERIC/docker-europeana-fulltext-aggregations) image,
which provides a python environment with logic to retrieve Europeana metadata dumps for newspaper collections, find 
associated full-text resources, and produce CMDI records that aggregate these resources at the level of newspaper title
and year. Like the harvester service, it is designed to run until completed, and its outputs become available in a
shared volume.

### Volumes and persistence
The following volumes are defined in the Docker Compose configurations in `clarin/*.yml`:

* Main data stores (included in backups):
  * `harvester-data` contains the output of successful `europeana-harvester` runs
  * `aggregator-data` contains the output of successful `europeana-fulltext-aggregator` runs
  * `joai-config` contains the configuration of the `joai` service
* Auxiliary:
  * `aggregator-input-storage`
  * `harvester-workdir`

## Usage

### With the CLARIN control script
These instruction use the 'single mode' of the control script. See the
[control script documentation](https://gitlab.com/CLARIN-ERIC/control-script/) for information on using the control
script on the server ('normal mode').

#### Initialisation and configuration

Check out the project, initialise the control script module and run the initialisation command of the control script:
```shell
git clone https://gitlab.com/CLARIN-ERIC/compose_europeana_oai.git
cd compose_europeana_oai
git submodule update --init -- control-script
./control.sh init
```

The script will ask to choose a deployment level. When it completes, you should have a symlink `clarin/.env` to an
existing file in the project's parent directory in place. Edit this file and set the following properties according to
the local requirements:

- `NGINX_PROXY_HTTPS_PORT`
  - the external HTTPS port on which the JOAI service and static content should be accessible 
- `NGINX_PROXY_HTTP_PORT`
  - the external HTTP port, which is **only** used for redirects to HTTPS
- `PUBLIC_BASE_URL`
  - the base URL through which the JOAI service is publicly accessible by **external end users**
- `JOAI_ADMIN_HTPASSWD_FILE`
  - Location (absolute path or relative to `clarin` direcotry) of an `htpasswd` file that defines username(s) and
password(s) for those who have access to the administrative parts of the JOAI web pages.
Suggested value `../../conf/nginx/htpasswd` is, which means that a file should be created in
the directory `${PROJECT_DIR}/conf/nginx` with the command `htpasswd -c htpasswd ${USERNAME}` where `${PROJECT_DIR}`
is the parent of the compose project directory, and `${USERNAME}` is the desired name of the user that has admin access.
The command will ask for a password, and store it in an encoded way in the `htpasswd` file.
- `EUROPEANA_API_KEY`
  - A Europeana API key is needed for full text aggregation and harvesting preflight. A key can be requested via
<https://pro.europeana.eu/page/apis> if needed.
- `HARVEST_PREFLIGHT_INCLUSION_SCRIPT`
  - Location (absolute path or relative to `clarin` directory) of a file defining a Bash function that determines which
collections should be included in the harvesting 'preflight' and with which filters. Use
`clarin/conf/europeana-preflight.inc.sh` as a template.
- `HARVEST_SET_LIST_INCLUSION_FILE=./conf/sets.inc.xml`
  - Location (absolute path or relative to `clarin` directory) of a file that specifies which sets should be included
in the harvest. Use `clarin/conf/sets.inc.xml` as a template.

There are several other properties that can be configured in `.env` but in most cases these can be left unconfigured
so as to use the defaults. See the file itself for documentation of all properties.

#### Starting and stopping the provider service
This will only be successful if the application is properly configured (see above)

```shell
./control.sh -s start
```

#### Running harvesting and aggregation jobs
This will only be successful if the application is properly configured (see above)
```shell
# Run the harvester using the current configuration (no arguments)
./control.sh -s run-harvester
# Run the full text aggregator (Europeana data set identifier(s) as argument(s))
./control.sh -s run-fulltext-aggregator 9200396 9200303
```

Note that these jobs run for a long time (typically hours to days) so make sure to run them in an environment that is
free from timeouts or interruptions.

#### Backup and restore

```shell
# Backup data and configuration
./control.sh -s backup
# Restore the latest backup
./control.sh -s restore latest
```

Note: the above will **fail** if data and/or configuration are still present in the volumes covered by the backup
```shell
# WARNING: the next step will remove all data and configuration. Do not run this unless needed!!
./control.sh -s down -V
# The next step should now successfully restore the latest backup 
./control.sh -s restore latest
```

### With docker compose
Go to the `clarin` directory and use the basic `docker-compose` to control the OAI-PMH provider and its proxy. Use
the additional configuration files in combination with the base configuration (`docker-compose.yml`) for the
harvester and aggregator services. Make sure that the `clarin/.env` file is initialised and properly configured.
Use `clarin/.env-template` as a template. See above for details on the configuration options.

Usage example:

```shell
cd clarin
# start oai-provider and proxy
docker-compose up
# run the harvester in the foreground
docker-compose \
  -f docker-compose.yml \
  -f harvester.yml \
  run --rm europeana-harvester
# run the 'retrieve', 'aggregate' and 'clean' stages of the full text aggregator on a collection in the foreground
docker-compose \
  -f docker-compose.yml \
  -f aggregator.yml \
  run --rm europeana-fulltext-aggregator retrieve aggregate clean 9200396
```
