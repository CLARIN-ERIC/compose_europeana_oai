#!/bin/bash
echo -n "Checking JOAI availability... "
if ! wait-for -t 5 joai:8080; then
  echo "Waiting for JOAI..."
  wait-for joai:8080
fi
echo "Connected to JOAI"

