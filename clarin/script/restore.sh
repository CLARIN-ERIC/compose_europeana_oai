#!/bin/bash
set -e

echo "RESTORE_FILE: ${RESTORE_FILE:?mandatory variable not set}"

#
# Process script arguments and parse all flags
#
for i in "$@"; do case $i in
    -v|--verbose)
        VERBOSE=1
        shift
        ;;
esac
done

if [ "${VERBOSE}" == '1' ]; then
	echo "VERBOSE: ${VERBOSE}"
	echo "RESTORE_FILE: ${RESTORE_FILE}"
	echo "TAR_OPTS: ${TAR_OPTS}"
fi

main() {
	if confirm_dir_is_empty '/data/joai-config' &&
	   confirm_dir_is_empty '/data/fulltext-aggregation' &&
	   confirm_dir_is_empty '/data/harvester-output'; then
	   do_restore
	else
	   echo 'One or more target directories are not empty or do not exist.'
	   echo 'Cannot restore backup, aborting!'
	   exit 1
	fi
}

do_restore() {
	echo 'Restoring backup'
	if [ -e "${RESTORE_FILE}" ]; then
		echo 'Decompressing backup archive'
		
		TAR_OPTS='zx'
		if [ "${VERBOSE}" == '1' ]; then
			TAR_OPTS="${TAR_OPTS}v"
		fi
		
		(cd '/data' && tar "${TAR_OPTS}" -f "${RESTORE_FILE}")
		echo 'Done'
	else
		echo 'Error: file to restore not found at expected location in container'
		echo "(${RESTORE_FILE})"
		exit 1
	fi
}

confirm_dir_is_empty() {
	if [ "${VERBOSE}" == '1' ]; then
		echo "Checking existence and emptiness of directory ${1}"
	fi
    if ! [ -d "$1" ]; then
		echo "directoy '${1}' does not exist"
    	return 1
    fi
    if [ "$(find "${1}" -type f|wc -l)" -ne 0 ]; then
		echo "Directoy '${1}' is not empty"
		return 1
	fi
	return 0
}

main
