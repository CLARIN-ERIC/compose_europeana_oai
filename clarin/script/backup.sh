#!/bin/bash
set -e

BACKUP_DIR='/clarin-backup'
DATA_DIR='/data'

echo "BACKUP_FILE_NAME: ${BACKUP_FILE_NAME:?mandatory variable not set}"

#############################################
#
# Process script arguments and parse all flags
#
for i in "$@"; do case $i in
    -v|--verbose)
        VERBOSE=1
        shift
        ;;
esac
done

if [ "${VERBOSE}" == '1' ]; then
	echo "VERBOSE: ${VERBOSE}"
fi

#############################################
echo "Copying data"

BACKUP_TMP_DIR=`mktemp -d -p "${BACKUP_DIR}"`
CP_OPTS='-ar'
if [ "${VERBOSE}" == '1' ]; then
	CP_OPTS="${CP_OPTS}v"
fi
cp "${CP_OPTS}" "${DATA_DIR}" "${BACKUP_TMP_DIR}/data"
cd "${BACKUP_TMP_DIR}/data"

#############################################
echo "Compressing data"

TAR_OPTS='zc'
if [ "${VERBOSE}" == '1' ]; then
	TAR_OPTS="${TAR_OPTS}v"
fi
tar "${TAR_OPTS}" -f "${BACKUP_DIR}/${BACKUP_FILE_NAME:-backup}.tgz" ./*

#############################################
echo "Cleaning up"
rm -rf "${BACKUP_TMP_DIR}"

echo "Done"
